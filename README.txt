--------------------------
    README FILE FOR
Snow Rendering Simulation
--------------------------

Author: Vasileios Katziouras
Email: sc13vk@leeds.ac.uk / den-paizei@hotmail.com
Version 1.0
Date: 11/05/2016

############################
Terminal Compilation Command
############################

For all machines using the g++ compiler the following command 
compiles the simulation.

(Has been tested on Windows 7/8.0/8.1, CentOS Linux, MacOS machines.)

        g++ SnowRendering.cpp imageloader.cpp -lGL -lGLU -lglut

After the compilation an executable will be created in the source 
directory.

################################
Simulation User Commands/Buttons
################################

'q':		        Pressing 'q' closes the simulation


'+':		        Pressing '+' increases the vertical 
                        velocity of the system

'-':		        Pressing '+' increases the vertical 
                        velocity of the system

Up arrow key:	        Pressing up zooms in the system


Down arrow key:         Pressing down zooms out of the system


Left arrow key:	        Pressing/Holding down left directs the 
                        system uniformly to the left of the screen

Right arrow key:        Pressing/Holding down right directs the 
                        system uniformly to the right of the screen

PageUp key:	        Pressing/Holding down PageUp directs the 
                        system uniformly towards the user

PageDown key:	        Pressing/Holding down PageDown directs the
                        system uniformly away from the user

-------------------------------------------------------------------------------

