/*
SnowRendering.cpp
Purpose: A realistic snowfall simulation rendered in real time

@author Vasileios Katziouras
@version 1.0 11/05/2016
*/


//Inclusions of needed header files

#include <iostream>
#include <vector>
#include <math.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include "imageloader.h"


//Global variable declaration and initialisation
using namespace std;
float frameCount;                                       //Frame counter
float previousTime;                                     //Previous frame time variable
float currentTime;                                      //Current frame time variable
float fps;                                              //Variable holding the calculated FPS rate
float windX = 0.0;				        //Wind on X axis
float sidewaysMultiplier = 50.0;                        //Multiplier for smooth horizontal movement
float descendMultiplier = 700.0;	                //Multiplier for smooth descend
float windZ = 0.0;			       		//Wind on Z axis
float velocity = 1.2;			        	//Vertical Velocity 
float zoom = -50.0;			       		//Zooming factor
float uniqueRand;			       		//Variable for accessing the same unique random value
int i;					       		//Loop variable
float gravity = 2.0;					//Gravity of the system
float windResult[3];                                    //Interpolation result
GLuint _textureId;			        	//TextureId holder
GLUquadric *disk;			        	//Sphere quadric variable


//Snow particle system class

class Snow
{
public:
	bool alive;					//Variable determining whether a particle is alive or not
	float timeUntilLaunch;			        //Variable determining when the particle will launch in the scene
	float life;					//The duration of a given particle being in the scene
	float fade;					//Measure of time deducted from the particle's life every time we redraw snow

	float randScaleX;			        //Snowflake scaling multiplier for the X axis of the flake
	float randScaleY;			        //Snowflake scaling multiplier for the Y axis of the flake

	float counter;				        //Small movement counter

	float tendencyX;			        //Tendency of movement of a flake in the X axis
	float tendencyY;				//Tendency of movement of a flake in the Y axis
	float tendencyZ;				//Tendency of movement of a flake in the Z axis

	float xpos;					//Position of flake in the X axis
	float ypos;				        //Position of flake in the Y axis
	float zpos;					//Position of flake in the Z axis

	float velocityX;				//Vertical velocity of a flake
	float velocityY;				//Vertical velocity of a flake
	float velocityZ;				//Vertical velocity of a flake
	float rotationTendency;				//Rotation tendency of particle
	float radius;					//Particle size determining radius
	float alpha;					//Particle alpha value determining transparency
	float gravity;					//Gravity force acting on flake

	GLuint texture;					//The texture of each flake

	int cubeNum;                                    //Placeholder value for calculation of the cuboid that the particle is in (grid partitioning)

};


//Corners of the blocks of the grid class and constructors

class Corner
{
public:
	Corner(float, float, float, float, float, float);
	Corner();

	float positionX;
	float positionY;
	float positionZ;

	float multiplierX;
	float multiplierY;
	float multiplierZ;
};

Corner::Corner(float x, float y, float z, float wx, float wy, float wz)
{
	positionX = x;
	positionY = y;
	positionZ = z;

	multiplierX = wx;
	multiplierY = wy;
	multiplierZ = wz;
}

Corner::Corner()
{
	positionX = 0;
	positionY = 0;
	positionZ = 0;

	multiplierX = 0;
	multiplierY = 0;
	multiplierZ = 0;
}

//Grid block class

class Block
{
public:
	Block(Corner, Corner, Corner, Corner, Corner, Corner, Corner, Corner);

	Corner corner1;
	Corner corner2;
	Corner corner3;
	Corner corner4;
	Corner corner5;
	Corner corner6;
	Corner corner7;
	Corner corner8;

	float volume;
};

//Block class constructor

Block::Block(Corner cor1, Corner cor2, Corner cor3, Corner cor4, Corner cor5, Corner cor6, Corner cor7, Corner cor8)
{
	corner1 = cor1;
	corner2 = cor2;
	corner3 = cor3;
	corner4 = cor4;
	corner5 = cor5;
	corner6 = cor6;
	corner7 = cor7;
	corner8 = cor8;
	volume = ((fabs(corner2.positionX - corner1.positionX)) *
		(fabs(corner1.positionY - corner3.positionY)) *
		(fabs(corner1.positionZ - corner5.positionZ)));
}

//Initialising corner values
Corner corner1(-80.0, 10.0, 80.0, 0.1, -0.3, -0.2);
Corner corner2(0.0, 10.0, 80.0, 0.1, -0.4, -0.3);
Corner corner3(80.0, 10.0, 80.0, 0.1, -0.2, -0.3);
Corner corner4(-80.0, -5.0, 80.0, 0.3, -0.2, -0.1);
Corner corner5(0.0, -5.0, 80.0, 0.2, -0.2, -0.1);
Corner corner6(80.0, -5.0, 80.0, 0.3, 0.4, 0.3);
Corner corner7(-80.0, -30.0, 80.0, 2.2, 1.5, -3.3);
Corner corner8(0.0, -30.0, 80.0, 3.3, 3.5, 3.3);
Corner corner9(80.0, -30.0, 80.0, 1.5, 1.5, -0.3);

Corner corner10(-80.0, 10.0, 0.0, 0.1, -0.5, 0.0);
Corner corner11(0.0, 10.0, 0.0, 0.1, -0.4, 0.0);
Corner corner12(80.0, 10.0, 0.0, 0.1, 0.0, 0.0);
Corner corner13(-80.0, -5.0, 0.0, 0.3, -0.2, -0.1);
Corner corner14(0.0, -5.0, 0.0, 0.4, -0.3, 0.1);
Corner corner15(80.0, -5.0, 0.0, 0.5, -1.4, -0.3);
Corner corner16(-80.0, -30.0, 0.0, 2.3, 3.3, -0.3);
Corner corner17(0.0, -30.0, 0.0, 9.3, 2.3, 0.3);
Corner corner18(80.0, -30.0, 0.0, 3.3, 2.3, 0.3);

Corner corner19(-80.0, 10.0, -80.0, 0.1, -0.1, 0.0);
Corner corner20(0.0, 10.0, -80.0, 0.1, -0.1, 0.0);
Corner corner21(80.0, 10.0, -80.0, 0.1, -0.1, 0.0);
Corner corner22(-80.0, -5.0, -80.0, 0.2, -0.1, 0.0);
Corner corner23(0.0, -5.0, -80.0, 0.2, -0.1, -0.1);
Corner corner24(80.0, -5.0, -80.0, 0.1, 0.1, 0.1);
Corner corner25(-80.0, -30.0, -80.0, 4.2, 0.4, -0.1);
Corner corner26(0.0, -30.0, -80.0, 4.2, 1.4, 0.1);
Corner corner27(80.0, -30.0, -80.0, 4.2, 1.4, 0.0);

//Giving each block its respective corner
Block grid1(corner1, corner2, corner4, corner5, corner10, corner11, corner13, corner14);
Block grid2(corner2, corner3, corner5, corner6, corner11, corner12, corner14, corner15);
Block grid3(corner4, corner5, corner7, corner8, corner13, corner14, corner16, corner17);
Block grid4(corner5, corner6, corner8, corner9, corner14, corner15, corner17, corner18);
Block grid5(corner10, corner11, corner13, corner14, corner19, corner20, corner22, corner23);
Block grid6(corner11, corner12, corner14, corner15, corner20, corner21, corner23, corner24);
Block grid7(corner13, corner14, corner16, corner17, corner22, corner23, corner25, corner26);
Block grid8(corner14, corner15, corner17, corner18, corner23, corner24, corner26, corner27);

//Initialisation of the system
Snow snowSystem[20000];


//Imageloading function for texture binding

GLuint loadTexture(Image* image)
{
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);

	return textureId;
}


//Function for enabling the user to:
//i.    Quit using the Q button
//ii.   Increase precipitation vertical velocity
//iii.  Decrease precipitation vertical velocity

void normal_keys(unsigned char key, int x, int y)
{
	if (key == 'q')
	{
		exit(0);
	}
	if (key == '+')
	{
		gravity += 1.0;
	}
	if (key == '-')
	{
		if (gravity >= 1.0)
		{
			gravity -= 1.0;
		}
	}
}


//Function for enabling the user to: 
//i.	Control the camera for zooming using the vertical arrows
//ii.	Control the wind in the X axis using the horizontal arrows
//iii.	Control the wind in the Z axis using the PgUp and PgDn buttons

void special_keys(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		zoom += 1.0;
	}
	if (key == GLUT_KEY_DOWN)
	{
		zoom -= 1.0;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		windX += 0.5;
	}
	if (key == GLUT_KEY_LEFT)
	{
		windX -= 0.5;
	}
	if (key == GLUT_KEY_PAGE_UP)
	{
		windZ += 0.5;
	}
	if (key == GLUT_KEY_PAGE_DOWN)
	{
		windZ -= 0.5;
	}
}

//Function that finds which cube a given particle is in (non-relative to the grid/hardcoded)

Block whichCube(float x, float y, float z)
{
	if (x>0)
	{
		if (y>5)
		{
			if (z<0)
			{
				return grid6;
			}
			else if (z >= 0)
			{
				return grid2;
			}
		}
		else if (y <= 5)
		{
			if (z<0)
			{
				return grid8;
			}
			else if (z >= 0)
			{
				return grid4;
			}
		}
	}
	else if (x <= 0)
	{
		if (y>5)
		{
			if (z<0)
			{
				return grid5;
			}
			else if (z >= 0)
			{
				return grid1;
			}
		}
		else if (y <= 5)
		{
			if (z<0)
			{
				return grid7;
			}
			else if (z >= 0)
			{
				return grid3;
			}
		}
	}
}


//Function that interpolates the wind values according to the particles position within the cubes

float * interpolate(float x, float y, float z)
{
	Block blockNum = whichCube(x, y, z);

	float volume1 = fabs((fabs(blockNum.corner1.positionX) - fabs(x)) *
		(fabs(blockNum.corner1.positionY) - fabs(y)) *
		(fabs(blockNum.corner1.positionZ) - fabs(z)));
	float volume2 = fabs((fabs(blockNum.corner2.positionX) - fabs(x)) *
		(fabs(blockNum.corner2.positionY) - fabs(y)) *
		(fabs(blockNum.corner2.positionZ) - fabs(z)));
	float volume3 = fabs((fabs(blockNum.corner3.positionX) - fabs(x)) *
		(fabs(blockNum.corner3.positionY) - fabs(y)) *
		(fabs(blockNum.corner3.positionZ) - fabs(z)));
	float volume4 = fabs((fabs(blockNum.corner4.positionX) - fabs(x)) *
		(fabs(blockNum.corner4.positionY) - fabs(y)) *
		(fabs(blockNum.corner4.positionZ) - fabs(z)));
	float volume5 = fabs((fabs(blockNum.corner5.positionX) - fabs(x)) *
		(fabs(blockNum.corner5.positionY) - fabs(y)) *
		(fabs(blockNum.corner5.positionZ) - fabs(z)));
	float volume6 = fabs((fabs(blockNum.corner6.positionX) - fabs(x)) *
		(fabs(blockNum.corner6.positionY) - fabs(y)) *
		(fabs(blockNum.corner6.positionZ) - fabs(z)));
	float volume7 = fabs((fabs(blockNum.corner7.positionX) - fabs(x)) *
		(fabs(blockNum.corner7.positionY) - fabs(y)) *
		(fabs(blockNum.corner7.positionZ) - fabs(z)));
	float volume8 = fabs((fabs(blockNum.corner8.positionX) - fabs(x)) *
		(fabs(blockNum.corner8.positionY) - fabs(y)) *
		(fabs(blockNum.corner8.positionZ) - fabs(z)));

	float wX = (volume1 * blockNum.corner8.multiplierX
		+ volume2 * blockNum.corner7.multiplierX
		+ volume3 * blockNum.corner6.multiplierX
		+ volume4 * blockNum.corner5.multiplierX
		+ volume5 * blockNum.corner4.multiplierX
		+ volume6 * blockNum.corner3.multiplierX
		+ volume7 * blockNum.corner2.multiplierX
		+ volume8 * blockNum.corner1.multiplierX) / blockNum.volume;
	float wY = (volume1 * blockNum.corner8.multiplierY
		+ volume2 * blockNum.corner7.multiplierY
		+ volume3 * blockNum.corner6.multiplierY
		+ volume4 * blockNum.corner5.multiplierY
		+ volume5 * blockNum.corner4.multiplierY
		+ volume6 * blockNum.corner3.multiplierY
		+ volume7 * blockNum.corner2.multiplierY
		+ volume8 * blockNum.corner1.multiplierY) / blockNum.volume;
	float wZ = (volume1 * blockNum.corner8.multiplierZ
		+ volume2 * blockNum.corner7.multiplierZ
		+ volume3 * blockNum.corner6.multiplierZ
		+ volume4 * blockNum.corner5.multiplierZ
		+ volume5 * blockNum.corner4.multiplierZ
		+ volume6 * blockNum.corner3.multiplierZ
		+ volume7 * blockNum.corner2.multiplierZ
		+ volume8 * blockNum.corner1.multiplierZ) / blockNum.volume;

	windResult[0] = wX;
	windResult[1] = wY;
	windResult[2] = wZ;
	return windResult;

}


//Initialising all particle variables

void initParticles(int i)
{
	snowSystem[i].alive = true;
	snowSystem[i].life = 10.0;
	snowSystem[i].fade = float(rand() % 100) / 1000.0 + 0.003;
	snowSystem[i].timeUntilLaunch = float(rand() % 50);

	snowSystem[i].xpos = (float)(rand() % 180) - 89.0 + ((float)(rand() % 10) / 10);
	snowSystem[i].ypos = ((float)(rand() % 50) / 10.0) + 60.0;
	snowSystem[i].zpos = (float)(rand() % 120) - 59.0 + ((float)(rand() % 10) / 10);

	snowSystem[i].radius = ((float)(rand() % 10) / 100) + 0.1;

	snowSystem[i].randScaleX = ((float)(rand() % 1000) / 1000) + 0.7;
	snowSystem[i].randScaleY = ((float)(rand() % 1000) / 1000) + 0.7;

	if (snowSystem[i].randScaleX >= snowSystem[i].randScaleY)
	{
		if (snowSystem[i].randScaleX - snowSystem[i].randScaleY <= 0.7)
		{
			snowSystem[i].radius = ((float)(rand() % 10) / 100) + 0.07;
		}
		else if (snowSystem[i].randScaleX - snowSystem[i].randScaleY > 0.7)
		{
			snowSystem[i].randScaleY += ((float)(rand() % 140) / 100);
			snowSystem[i].radius = ((float)(rand() % 10) / 100) + 0.1;
		}
	}
	else if (snowSystem[i].randScaleX < snowSystem[i].randScaleY)
	{
		if (snowSystem[i].randScaleY - snowSystem[i].randScaleX <= 0.7)
		{
			snowSystem[i].radius = ((float)(rand() % 10) / 100) + 0.07;
		}
		else if (snowSystem[i].randScaleY - snowSystem[i].randScaleX > 0.7)
		{
			snowSystem[i].randScaleX += ((float)(rand() % 140) / 100);;
			snowSystem[i].radius = ((float)(rand() % 10) / 100) + 0.1;
		}
	}

	snowSystem[i].tendencyX = ((float)(rand() % 10) / 10) - 0.3;
	snowSystem[i].tendencyY = -3 - ((float)(rand() % 10));
	snowSystem[i].tendencyZ = ((float)(rand() % 100) / 100) - 0.05;

	snowSystem[i].velocityX = 0.0;
	snowSystem[i].velocityY = 5.0;
	snowSystem[i].velocityZ = 0.0;

	snowSystem[i].rotationTendency = (float)(rand() % 90);

	snowSystem[i].alpha = 0.65f + ((float)(rand() % 20)/100);

	snowSystem[i].texture = _textureId;
	snowSystem[i].counter = 0;
}


//Calling initialisation for all particles in the system and binding textures on them

void init()
{
	Image* image = loadBMP("snowflake.bmp");
	_textureId = loadTexture(image);
	delete image;

	for (i = 0; i < 20000; i++)
	{
		initParticles(i);
	}
}


//We draw all particles in the scene at the appropriate position

void drawSnow()
{
	float x, y, z;
	for (i = 0; i < 20000; i = i + 1)
	{
		if ((snowSystem[i].alive == true) && (snowSystem[i].timeUntilLaunch <= 0.0))
		{
			snowSystem[i].counter = snowSystem[i].counter + (int)(rand() % 5);

			x = snowSystem[i].xpos;
			y = snowSystem[i].ypos;
			z = snowSystem[i].zpos + zoom;

			glPushMatrix();
			glTranslatef(x, y, z);
			glRotatef(snowSystem[i].rotationTendency, 0.0, 0.0, 1.0);
			glScalef(snowSystem[i].randScaleX, snowSystem[i].randScaleY, 0);
			glShadeModel(GL_SMOOTH);
			glEnable(GL_BLEND); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glColor4f(1.0f, 1.0f, 1.0f, snowSystem[i].alpha);
			disk = gluNewQuadric();
			gluQuadricDrawStyle(disk, GLU_FILL);
			gluQuadricTexture(disk, GL_TRUE);
			gluQuadricNormals(disk, GLU_SMOOTH);
			gluDisk(disk, 0.0, snowSystem[i].radius, 6, 2);
			glEnable(GL_TEXTURE_2D);
			gluQuadricTexture(disk, 1);
			glBindTexture(GL_TEXTURE_2D, snowSystem[i].texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			
			/*glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			glEnable(GL_COLOR_MATERIAL);
			GLfloat ambient[] = { 1.0f, 1.0f, 1.0f, snowSystem[i].alpha };                 ##Attempt to introduce lighting in the scene######
			GLfloat diffuse[] = { 1.0f, 1.0f, 1.0f, snowSystem[i].alpha };                 ##did not work out so well in Linux machines######
			glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);                                   ##since the particles appear to have a different##
			glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);                                   #######colour than the one specified##############
			*/
			
			glPopMatrix();

			float* wind = interpolate(snowSystem[i].xpos, snowSystem[i].ypos, snowSystem[i].zpos);

			snowSystem[i].velocityX += (snowSystem[i].tendencyX + (wind[0] / 10))*velocity;
			snowSystem[i].velocityY += snowSystem[i].tendencyY - gravity + (wind[1] / 10);
			snowSystem[i].velocityZ += (snowSystem[i].tendencyZ + (wind[2] / 10))*velocity;

			if (snowSystem[i].counter >= 20)
			{
				snowSystem[i].tendencyX += (0.1 - ((float)(rand() % 20) / 100));
				snowSystem[i].tendencyZ += (0.1 - ((float)(rand() % 20) / 100));

				uniqueRand = (float)(rand() % 300);
				descendMultiplier += uniqueRand;
				snowSystem[i].xpos += (snowSystem[i].velocityX + windX) / (sidewaysMultiplier * 3);
				snowSystem[i].ypos += snowSystem[i].velocityY / (descendMultiplier * 2);
				snowSystem[i].zpos += (snowSystem[i].velocityZ + windZ) / (sidewaysMultiplier * 3);
				descendMultiplier -= uniqueRand;
				snowSystem[i].rotationTendency += (float)(rand() % 100) - 50.0;

				snowSystem[i].counter = 0;
			}
			else
			{
				snowSystem[i].xpos += (snowSystem[i].velocityX + windX) / (sidewaysMultiplier * 4);
				snowSystem[i].ypos += snowSystem[i].velocityY / (descendMultiplier * 3);
				snowSystem[i].zpos += (snowSystem[i].velocityZ + windZ) / (sidewaysMultiplier * 4);
				snowSystem[i].rotationTendency += (float)(rand() % 50) - 25;
			}

			snowSystem[i].life -= snowSystem[i].fade;

			if ((snowSystem[i].ypos <= -20.0) || (snowSystem[i].xpos <= -150.0) || (snowSystem[i].zpos <= -150.0)
				|| (snowSystem[i].xpos >= 150.0) || (snowSystem[i].xpos >= 150.0))
			{
				snowSystem[i].life = -1.0;
			}

			if (snowSystem[i].life < 0.0)
			{
				initParticles(i);
				snowSystem[i].timeUntilLaunch = float(rand() % 1);
			}
		}
		else
		{
			snowSystem[i].timeUntilLaunch -= 0.2;
		}
	}
}

//We draw all appropriate objects in the scene (simple terrain and calling drawSnow to add the particles)

void drawScene()
{
	int i, j;
	float x, y, z;

	glShadeModel(GL_SMOOTH);
	glClearColor(0.29, 0.29, 0.35, 0.3);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glColor3f(0, 0.08, 0);

	glBegin(GL_TRIANGLES);
	glVertex3f(-500, -10, -1000 + zoom);
	glVertex3f(-500, -10, 1000 + zoom);
	glVertex3f(500, -10, 1000 + zoom);
	glVertex3f(-500, -10, -1000 + zoom);
	glVertex3f(500, -10, -1000 + zoom);
	glVertex3f(500, -10, 1000 + zoom);

	glEnd();
	drawSnow();
	glutSwapBuffers();

}


//Function to take care of reshaping the window of the simulation

void reshape(int w, int h)
{
	if (h == 0) h = 1;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45, (float)w / (float)h, .1, 200);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//Frames per second data collection function

void calculateFPS()
{
	//  Increase frame count
	frameCount++;

	//  Get the number of milliseconds since glutInit called
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	currentTime = glutGet(GLUT_ELAPSED_TIME);

	//  Calculate time passed
	int timeInterval = currentTime - previousTime;

	if (timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps = frameCount / (timeInterval / 1000.0f);
		cout << currentTime << " ";
		cout << fps << '\n';
		//  Set time
		previousTime = currentTime;

		//  Reset frame count
		frameCount = 0;
	}
}

//Idle function for frame updating

void idle()
{
	calculateFPS();
	glutPostRedisplay();
}


//Main function for calling all accessory functions, setting parameters and running the simulation

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(1024, 768);
	glutCreateWindow("Snow Rendering");
	init();
	glutDisplayFunc(drawScene);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(normal_keys);
	glutSpecialFunc(special_keys);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}

